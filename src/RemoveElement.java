public class RemoveElement {
    static void removeElement(int[] arr,int val){
        for(int i=0; i<arr.length; i++){
            if(i==arr.length-1){
                break;
            }

            if(arr[i]==val){
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            }
        }

        System.out.print("[");
        for(int j=0; j<arr.length; j++){
            if(arr[j]!=val){
                if(j==arr.length-1){
                    System.out.print(arr[j]);
                }else{
                    System.out.print(arr[j]+",");
                }
            }else{
                if(j==arr.length-1){
                    System.out.print("_");
                }else{
                    System.out.print("_"+",");
                }
            }
        }
        System.out.println("]");

    }
    public static void main(String[] args){
        int[] nums = {0,1,2,2,3,0,4,2};
        int val = 2;
        System.out.print("nums = ");
        removeElement(nums, val);
        int number = 0;
        for(int i=0; i<nums.length; i++){
            if(nums[i]!=val){
                number++;
            }
        }
        System.out.println(number);

    }
}
